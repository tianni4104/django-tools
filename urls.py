from django.conf.urls import url
from . import views

urlpatterns = [

    url(r'^tables/$', views.tables, name='tables'),
    url(r'^editable/$', views.editable, name='editable'),
    url(r'^addRow/$', views.addRow, name='editable'),
    url(r'^forms/$', views.forms, name='forms'),
]