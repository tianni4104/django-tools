# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-15 19:50
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_tools', '0002_auto_20161031_1456'),
    ]

    operations = [
        migrations.DeleteModel(
            name='todo',
        ),
    ]
