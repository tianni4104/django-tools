import pandas as pd
import numpy as np
import time

class TableBase(object):

    #html setting
    attrs = {'table': '',
             'thead': '',
             'tbody': '',
             'tr': '',
             'th': '',
             'td': ''}

    #urls
    ######

    ######

    columns_order = []
    sortable = []
    editable = {}
    searchable = {}
    formattable = []

    #data setting
    model_name='NAN'
    app_name='NAN'
    sheet_name='NAN'
    pk=''

    data = pd.DataFrame(data=['empty'],columns=['Empty'])

    #initilize data
    def __init__(self):
        self.data=self.order_columns(self.data,self.columns_order)
        self.index=self.get_index(self.data,self.pk)
        self.columns=self.get_columns(self.data)

        self.auto = {
                     'table':'',
                     'thead':'',
                     'tbody':'',
                     'tr':'',
                     'th':'',
                     'td':'',
                     }

        self.init_auto()

    def init_auto(self):
        self.shape = self.data.shape

        #init th
        self.auto['th']=np.dstack((self.columns, self.auto_th_attrs()))

        #init td
        self.auto['td']=np.dstack((self.data.values, self.auto_td_attrs()))

    def order_columns(self,data,columns_order):

        if columns_order and len(columns_order)==len(data.columns):
            data = data.reindex_axis(columns_order, axis=1)
        return data

    def get_index(self,data,pk):
        if pk:
            index = data[pk].values
        else:
            index = data.index.tolist()

        return index

    def get_columns(self,data):
        columns = data.columns.tolist()
        return columns

    def auto_th_attrs(self):
        # t0=time.clock()
        attrs=pd.DataFrame(columns=self.columns,index=['attrs','sortable','editable','searchable','formattable']).fillna('')

        # init data-field
        for i in attrs.columns.values:
            attrs.loc['attrs',i]=' data-field={field_name}'.format(field_name=i)

        #init sortable

        sortable_columns=list(set(self.sortable) & set(self.columns))
        if sortable_columns:
            attrs.loc['sortable',sortable_columns]=' data-sortable="true"'

        #init editable

        editable_columns=list(set(self.editable) & set(self.columns))
        if editable_columns:
            for i in editable_columns:
                if self.editable[i]['type'] == 'text':
                    attrs.loc['editable',i]=' data-editable="true"' \
                                            ' data-editable-type="text"'\
                                            ' data-editable-title={title}'.format(title=self.editable[i]['title'])

                if self.editable[i]['type'] == 'select':
                    attrs.loc['editable',i]=' data-editable="true"' \
                                            ' data-editable-type="select"' \
                                            ' data-editable-title="{title}"'.format(title=self.editable[i]['title'])\
                                           +' data-editable-source={source}'.format(source=self.editable[i]['source'])

        #init searchable
        searchable_columns=list(set(self.searchable.keys()) & set(self.columns))
        if searchable_columns:
            for i in searchable_columns:
                attrs.loc['searchable',i]=' data-filter-control="{type}"'.format(type= self.searchable[i])

        #init formattable
        formattable_columns=list(set(self.formattable) & set(self.columns))
        if formattable_columns:
            for i in formattable_columns:
                attrs.loc['formattable',i]=' data-formatter=' + str(i) + 'Formatter'

        attrs.loc['attrs',:] +=attrs.loc['sortable',:]\
                              +attrs.loc['editable',:]\
                              +attrs.loc['searchable',:]\
                              +attrs.loc['formattable',:]

        # print(time.clock()-t0)
        return attrs.loc['attrs',:].values

    # create unique id and other attrs for each table data
    def auto_td_attrs(self):
        # t0=time.clock()
        td_attrs = pd.DataFrame(columns=self.columns, index=self.index).fillna('')
        for j in range(td_attrs.shape[1]):
            for i in range(td_attrs.shape[0]):
                td_attrs.iat[i, j] += ' id="' + '-'.join(
                    [self.model_name,self.app_name, self.sheet_name,self.pk, str(td_attrs.index[i]), str(td_attrs.columns[j])])+'"'
        # print(time.clock()-t0)
        return td_attrs.values
