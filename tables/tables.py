from ..tables.TableBase import TableBase
from ..models.models import todo
import pandas as pd
import numpy as np

class try_table(TableBase):

    # html setting
    attrs = {'table': 'class="table table-bordered table-striped"',
             'thead': '',
             'tbody': '',
             'tr': '',
             'th': '',
             'td': ''}

    columns_order=['symbol','price']
    editable=['price']

    # data setting
    model_name='default'
    sheet_name='todo'
    pk='symbol'

    def __init__(self):

        self.model = todo.objects.all().values('symbol', 'price')
        TableBase.data =pd.DataFrame.from_records(self.model)
        TableBase.__init__(self)








