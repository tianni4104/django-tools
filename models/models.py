from django.db import models
from django.utils.encoding import python_2_unicode_compatible
# Create your models here.

@python_2_unicode_compatible
class todo(models.Model):

    symbol=models.CharField(primary_key=True,max_length=100)
    price=models.FloatField()

    def __str__(self):
        return str(self.symbol)

    class Meta:
        ordering=['symbol']




