


def get_username(request):
    user = request.user
    if user.is_authenticated():
        username = user.get_username()
    else:
        username = "Please sign in !"
    return username